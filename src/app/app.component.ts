import {Component, HostListener, OnInit} from '@angular/core';

interface GameModal {
  green: boolean;
  red: boolean;
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  height;
  width;
  gameOverMsg = 'Please start game...';
  greenArry = [];
  greenArryCount = this.height;
  gameLayout = [];
  FinalScore;
  numberOfSteps = 0;
  centerPoint = {i: Math.round(this.height / 2), j: Math.round(this.width / 2)};
  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {

    if (this.gameLayout.length < 1) {
      return false;
    }
    this.gameLayout[this.centerPoint.i][this.centerPoint.j].green = false;
    this.gameLayout[this.centerPoint.i][this.centerPoint.j].red = false;
    switch (event.code) {
      case 'ArrowUp' : {
        if (this.centerPoint.i !== 0) {
          this.centerPoint.i--;
        } else {
          this.gameLayout[this.centerPoint.i][this.centerPoint.j].red = true;
          return false;
        }
        break;
      }
      case 'ArrowDown' : {
        if (this.centerPoint.i !== this.height - 1) {
          this.centerPoint.i++;
        } else {
          this.gameLayout[this.centerPoint.i][this.centerPoint.j].red = true;
          return false;
        }
        break;
      }
      case 'ArrowRight' : {
        if (this.centerPoint.j !== this.height - 1) {
          this.centerPoint.j++;
        } else {
          this.gameLayout[this.centerPoint.i][this.centerPoint.j].red = true;
          return false;
        }
        break;
      }
      case 'ArrowLeft' : {
        if (this.centerPoint.j !== 0) {
          this.centerPoint.j--;
        } else {
          this.gameLayout[this.centerPoint.i][this.centerPoint.j].red = true;
          return false;
        }
        break;
      }
    }
    if (this.gameLayout[this.centerPoint.i][this.centerPoint.j].green) {
      this.greenArryCount--;
      this.gameLayout[this.centerPoint.i][this.centerPoint.j].green = false;
      if (this.greenArryCount  === 0) {
        this.gameOverMsg  = 'Game Over, Please start again...';
        this.FinalScore = this.numberOfSteps;
        setTimeout(() => {
          alert('Game Over, Final Score:=>' + this.FinalScore);
          },
          10);
        this.deconstruct();
        return false;
      }
    }
    this.gameLayout[this.centerPoint.i][this.centerPoint.j].red = true;
    this.numberOfSteps++;
  }
  constructor() {
  }
  ngOnInit() {

  }
  genrateLayout() {
    this.gameOverMsg  = 'Game Inprogress...';
    this.createRandomNumber();
    this.createLayout();
  }
  createRandomNumber() {
    this.greenArryCount = this.height;
    this.centerPoint = {i: Math.round(this.height / 2), j: Math.round(this.width / 2)};
    while (this.greenArry.length !== this.height) {
      const iVar = Math.floor(Math.random() * this.height);
      const jVar = Math.floor(Math.random() * this.width);
      const findVal = this.greenArry.find((x) => ((x.i === iVar && x.j === jVar)
        || (x.i === this.centerPoint.i && x.j === this.centerPoint.j)));
      if (!findVal) {
        this.greenArry.push({i: iVar, j: jVar});
      }
    }
  }
  createLayout() {
    for (let i = 0; i < (this.height); i++) {
      this.gameLayout.push([]);
      for (let j = 0; j < (this.width); j++) {
        const temp: GameModal = {green: false, red: false};
        temp.green = false;
        temp.red = false;
        this.gameLayout[i][j] = temp;
      }
    }
    for (let i = 0; i < this.greenArry.length; i++) {
          this.gameLayout[this.greenArry[i].i][this.greenArry[i].j].green = true;
          this.gameLayout[this.greenArry[i].i][this.greenArry[i].j].red = false;
    }
    this.gameLayout[this.centerPoint.i][this.centerPoint.j].green = false;
    this.gameLayout[this.centerPoint.i][this.centerPoint.j].red = true;
    if (this.gameLayout[this.centerPoint.i][this.centerPoint.j].green) {
      this.greenArryCount--;
    }
  }
  deconstruct() {
    this.gameLayout = [];
    this.greenArry = [];
    this.greenArryCount = 0;
    this.height = 0;
    this.width = 0;
    this.numberOfSteps = 0;
  }
}

